import { Component, OnInit, AfterContentInit, AfterViewInit } from '@angular/core';
import Swiper from 'swiper';
import { TimeoutError } from 'rxjs';

@Component({
  selector: 'app-first-page',
  templateUrl: './first-page.component.html',
  styleUrls: ['./first-page.component.css']
})
export class FirstPageComponent implements AfterViewInit {

  testSwiper: Swiper;
  slides = [
    'assets/images/book1.jpg',
    'assets/images/book2.jpg',
  ];

  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.testSwiper = new Swiper('.swiper-container', {
      direction: 'horizontal',
      loop: true,
      pagination: {
        el: '.swiper-pagination',
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      scrollbar: {
        el: '.swiper-scrollbar',
      },
    });
  }

}
