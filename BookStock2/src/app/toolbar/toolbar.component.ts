import { Component, OnInit, Input } from '@angular/core';
import { ThemePalette } from '@angular/material/core';
import { Navlink } from '../model/navlink';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit {

  public navlink: Array<Navlink>;

  constructor() { }

  ngOnInit() {
    this.navlink = [
      new Navlink('關於Reader'),
      new Navlink('書籍推薦'),
      new Navlink('文章發表'),
      new Navlink('聯絡我')
    ];
  }

}
