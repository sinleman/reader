import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Stock } from './model/stock';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
  // encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {
  title = 'stock-market';

  public stock: Stock;
  private counter = 1;


  ngOnInit(): void {
    this.stock = new Stock('Test Stock Company', 'TSC', 85, 80, 'NASDAQ');
  }

  onToggleFavorite(stock: Stock) {
    console.log('Favorite for stock', stock, ' was triggered');
    this.stock.favorite = !this.stock.favorite;
  }

  changeStockObject() {
    this.stock = new Stock('Test Stock Company -' + this.counter++, 'TSC', 85, 80, '');
  }

  changeStockPrice($event) {
    console.log($event);
    this.stock.price += 10;
  }

}
