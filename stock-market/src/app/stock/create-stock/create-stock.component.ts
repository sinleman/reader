import { Component, OnInit } from '@angular/core';
import { Stock } from 'src/app/model/stock';

@Component({
  selector: 'app-create-stock',
  templateUrl: './create-stock.component.html',
  styleUrls: ['./create-stock.component.css']
})
export class CreateStockComponent {

  public stock: Stock;
  public confirmed = false;
  public exchanges =[
    'NYSE',
    'NSDAQ',
    'OTHER'
  ];

  constructor() {
    this.stock = new Stock('test', '', 0, 400, 'NASDAQ');
  }

  setStockPrice(price) {
    this.stock.price = price;
    this.stock.previousPrice = price;
  }

  createStock(stockForm) {
    console.log('Create stock ', stockForm);
    if(stockForm.valid) {
      console.log('Creat stock', this.stock);
    }else {
      console.error('Stock form is in an invalid state');
    }
  }

}
