import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { Stock } from '../model/stock';

@Component({
  selector: 'app-stock-item',
  templateUrl: './stock-item.component.html',
  styleUrls: ['./stock-item.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StockItemComponent implements OnInit {

  @Input() public stock: Stock;
  @Output() private toggleFavorite: EventEmitter<Stock>;

  constructor() {
    this.toggleFavorite = new EventEmitter<Stock>();
  }

  ngOnInit() {
  }

  onToggleFavorite($event) {
    // this.stock.favorite = !this.stock.favorite;
    this.toggleFavorite.emit(this.stock);
  }

  trackStockByCode(index, stock) {
    return stock.code;
  }

  changeStockPrice() {
    this.stock.price += 5 ;
  }

}
